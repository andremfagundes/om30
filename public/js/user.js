var User = {
    init: function() {

        $('#cpf').blur(function() {
            if ($(this).val()) {
                User.validarCPF($(this).val());
            }
        });

        $('#cns').blur(function() {
            if ($(this).val()) {
                console.log('1');
                User.validarCns($(this).val());
            }
        });

        $('#cep').blur(function() {
            if ($(this).val()) {
                User.buscarCep($(this).val());
            }
        });

        if ($("#id_user").val() == '') {
            User.bloquearCamposEndereco();
        }

        User.criarMascaras();

    },
    validarCep: function(cep) {

        var objER = /^[0-9]{8}$/;
        strCEP = cep.trim();

        if (strCEP.length > 0) {
            if (objER.test(cep)) {
                User.habilitarCamposEndereco();
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },
    buscarCep: function(cep) {

        //Nova variável "cep" somente com dígitos.
        var cep = cep.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {
            //Valida o formato do CEP.
            if (this.validarCep(cep)) {

                $("#rua,#bairro,#cidade,#estado").val("...");

                //consultando cep no viacep
                $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $('#rua').val(dados.logradouro);
                        $('#bairro').val(dados.bairro);
                        $('#cidade').val(dados.localidade);
                        $('#estado').val(dados.uf);
                    } //end if.
                    else {
                        //se cep não encontrado
                        User.habilitarCamposEndereco();
                        User.limpaCamposEndereco();
                        $("#rua").focus();
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                User.limpaCamposEndereco(true);
                alert("CEP inválido!");
            }
        } //end if.

    },
    validarCns: function(cns) {
        if (!User.validacaoCns(cns)) {
            alert('CNS inválido!');
            $('#cns').val('').focus();
        }
    },
    validacaoCns: function(cnsValidar) {
        var cns = cnsValidar.replace(/\D/g, '');
        var validTamanho = cns.length == 15;
        var validInicio = ['1', '2', '7', '8', '9'].includes(cns[0]);

        if (validTamanho && validInicio) {
            if (['1', '2'].includes(cns[0])) {
                //CNS Iniciados em 1, 2
                var pis = cns.substring(0, 11);
                var soma = pis.split('').reduce((total, value, index) => total + (value * (15 - index)), 0);
                var resto = soma % 11;
                var dv = resto == 0 ? 0 : 11 - resto;

                var resultado = dv == 10 ? `${pis}001${(11 - ((soma + 2) % 11))}` : `${pis}000${dv}`
                return resultado == cns;

            } else {
                //cns que inicia com 7, 8 ou 9
                var soma = cns.split('').reduce((total, value, index) => total + (value * (15 - index)), 0);
                return soma % 11 == 0;
            }
        } else {
            return false;
        }
    },
    validarCPF: function(cpfValue) {
        var cpfValid = true;
        cpf = cpfValue.replace(/\D/g, '');

        if (!(cpf.toString().length != 11 || /^(\d)\1{10}$/.test(cpf))) {
            [9, 10].forEach(function(j) {
                var soma = 0,
                    r;
                cpf.split(/(?=)/).splice(0, j).forEach(function(e, i) {
                    soma += parseInt(e) * ((j + 2) - (i + 1));
                });
                r = soma % 11;
                r = (r < 2) ? 0 : 11 - r;
                if (r != cpf.substring(j, j + 1)) {
                    cpfValid = false;
                }
            });
        } else {
            cpfValid = false;
        }

        if (!cpfValid) {
            alert('CPF inválido!');
            $('#cpf').val('');
        }
    },
    criarMascaras: function() {
        $('#cep').mask("99.999-999");
        $('#cpf').mask("999.999.999-99");
        $('#cns').mask("999 9999 9999 9999");
        $('#dt_nascimento').mask("99/99/9999");
    },
    limpaCamposEndereco: function(boolLimparCep = false) {
        $('#rua,#bairro,#cidade,#estado').val('');
        if (boolLimparCep) {
            $("#cep").val('');
        }
        $('#cep').focus();
    },
    bloquearCamposEndereco: function() {
        $('#rua,#complemento,#bairro,#cidade,#estado').prop("disabled", true);
    },
    habilitarCamposEndereco: function() {
        $('#rua,#complemento,#bairro,#cidade,#estado').prop("disabled", false);
    }

}