<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Address extends Migration
{
	public function up()
	{
		$this->forge->addField([
					'id'          => [
							'type'           => 'INT',
							'unique'		 => true,
							'unsigned'       => true,
							'auto_increment' => true,
					],
					'user_id'          => [
							'type'           => 'INT',
							'unsigned'       => true,
					],
					'cep'       => [
							'type'       => 'VARCHAR',
							'constraint' => '15',
					],
					'rua' => [
							'type' => 'VARCHAR',
							'constraint' => '250',
					],
					'complemento' => [
							'type' => 'VARCHAR',
							'constraint' => '100',

					],
					'bairro' => [
							'type' => 'VARCHAR',
							'constraint' => '100',
					],
					'cidade' => [
							'type' => 'VARCHAR',
							'constraint' => '100',
					],
					'estado' => [
							'type' => 'VARCHAR',
							'constraint' => '30',
					]	
			]);

			$this->forge->addPrimaryKey('id');
			$this->forge->addForeignKey('user_id','user','id','CASCADE','CASCADE');
			$this->forge->createTable('address', true);
	}

	public function down()
	{
		$this->forge->dropTable('address', true);
	}
}
