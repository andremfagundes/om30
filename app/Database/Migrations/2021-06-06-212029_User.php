<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class User extends Migration
{
	public function up()
	{
		$this->forge->addField([
					'id'	=> [
						'type'           => 'INT',
						'unique'		 => true,
						'unsigned'       => true,
						'auto_increment' => true,
					],
					'nome'	=> [
							'type'       => 'VARCHAR',
							'constraint' => '100',
					],
					'foto'	=> [
							'type' => 'TEXT',
							'null' => true,
					],
					'dt_nascimento' => [
							'type' => 'DATE',		
					],
					'nome_mae' => [
							'type' => 'VARCHAR',
							'constraint' => '100'
					],
					'cpf' => [
							'type' => 'VARCHAR',
							'constraint' => '11',
						    'unique'     => true
					],
					'cns' => [
							'type' => 'VARCHAR',
							'unique'     => true
					],
					'created_at' => [
							'type' => 'DATE'
					],
					'updated_at' => [
							'type' => 'DATE'
					],
					'deleted_at' => [
							'type' => 'DATE',
							'null' => true,
					]
		]);
			$this->forge->addPrimaryKey('id', true);
			$this->forge->createTable('user', true);
	}

	public function down()
	{
		$this->forge->dropTable('user', true);
	}
}
