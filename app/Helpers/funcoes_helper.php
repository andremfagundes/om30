<?php

/**
 * Limpa campo de CPF(tira a formatação)
 */
function limparFormatacao($valor) : string {
    $valor = preg_replace('/[^0-9]/', '', $valor);
    return $valor;
}

function formatarDataVisualizacao($data){
    return date('d/m/Y', strtotime($data));
}

function formatarCPF($cpf){
            $bloco_1 = substr($cpf,0,3);
            $bloco_2 = substr($cpf,3,3);
            $bloco_3 = substr($cpf,6,3);
            $dig_verificador = substr($cpf,-2);
            return $bloco_1.".".$bloco_2.".".$bloco_3."-".$dig_verificador;
}
