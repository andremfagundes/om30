<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'user';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDeletes       = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'nome',
		'nome_mae',
		'dt_nascimento',
		'cpf',
		'cns'
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [
		'nome'			=> 'required',
		'nome_mae'		=> 'required',
		'dt_nascimento' => 'required',
		'cpf'     		=> 'required',
		'cns'			=> 'required|validCNS'
	];
	protected $validationMessages   = [
		'nome' 			=> ['required' =>'Campo Nome é Obrigatório'],
		'nome_mae' 		=> ['required' =>'Campo Nome da Mãe é Obrigatório'],
		'dt_nascimento' => ['required' =>'Campo Data de Nascimento é Obrigatório'],
		'cpf' 			=> ['required' =>'Campo CPF é Obrigatório'],
		'cns' 			=> ['required' =>'Campo CNS é Obrigatório','validCNS' =>'CNS inválido'],
	];

	protected $skipValidation = false;
	
}
