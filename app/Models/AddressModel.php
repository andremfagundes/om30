<?php

namespace App\Models;

use CodeIgniter\Model;

class AddressModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'address';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDeletes       = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'user_id',
		'cep',
		'rua',
		'complemento',
		'bairro',
		'cidade',
		'estado',
	];

	// Dates
	protected $useTimestamps        = false;

	// Validation
	protected $validationRules      = [
		'cep'			=> 'required',
		'rua'			=> 'required',
		'bairro'		=> 'required',
		'cidade' 		=> 'required',
		'estado'     	=> 'required',
	];

	protected $validationMessages   = [
		'cep' 			=> ['required' =>'Campo cep é Obrigatório'],
		'rua' 			=> ['required' =>'Campo rua é Obrigatório'],
		'bairro' 		=> ['required' =>'Campo bairro é Obrigatório'],
		'cidade' 		=> ['required' =>'Campo cidade é Obrigatório'],
		'estado' 		=> ['required' =>'Campo estado é Obrigatório']
	];

	protected $skipValidation = false;

	/**
	 * Salva e altera endereço
	 * @author André Maciel Fagundes
	 * @version 1.0
	 * @package App\Models
	 */
	public function saveAddress($addressPost){

		if(isset($addressPost['id'])) $address['id'] = $addressPost['id'];

		$address['user_id']			= $addressPost['user_id'];
		$address['cep'] 			= limparFormatacao($addressPost['cep']);
		$address['rua'] 			= $addressPost['rua'];
		$address['complemento'] 	= $addressPost['complemento'];
		$address['bairro'] 			= $addressPost['bairro'];
		$address['cidade'] 			= $addressPost['cidade'];
		$address['estado'] 			= $addressPost['estado'];

		return $this->save($address);
	}
}
