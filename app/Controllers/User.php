<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;
use App\Models\AddressModel;
use CodeIgniter\Database\Query;

class User extends BaseController
{
	private $userModel;

	public function __construct(){
		$this->userModel = new UserModel();
		$this->session   =  \Config\Services::session();
	}
	
	/**
	 * Listagem de Paciente
	 * @author André Maciel Fagundes
	 * @version 1.0
	 * @package App\Controllers
	 */
	public function index()
	{
		return view('users', [
			'users' => $this->userModel->select('user.*,address.rua,address.bairro,address.estado')
                  ->join('address', 'address.user_id = user.id')
                  ->paginate(10),
			'pager' => $this->userModel->pager
		]);
	}

	public function create(){
		return view('form_user');
	}

	/**
	 * Cadastro de Paciente
	 * @author André Maciel Fagundes
	 * @version 1.0
	 * @package App\Controllers
	 */
	public function register(){

		if($this->request->getMethod() === 'post'){

			try{
				$userPost = $this->request->getPost('user');

				$this->userModel->transBegin();
				
				$user['cpf'] 			= limparFormatacao($userPost['cpf']);
				$user['cns'] 			= $userPost['cns'];
				$user['nome'] 			= $userPost['nome'];
				$user['dt_nascimento'] 	= $userPost['dt_nascimento'];
				$user['nome_mae'] 		= $userPost['nome_mae'];
				
				if($userId = $this->userModel->insert($user)){

					$this->addressModel = new AddressModel();

					$addressPost = $this->request->getPost('address');
					$addressPost['user_id'] = $userId;
					
					if($this->addressModel->saveAddress($addressPost)){
						$this->session->setFlashdata("message", "Paciente salvo com sucesso!");
					}else{
						$this->userModel->transRollback();
						$this->session->setFlashdata("error", "Não foi possível salvar paciente! Tente novamente!");

						return view('form_user',[
							'errors' => $this->userModel->errors()
						]);
					}
				}else{
					$this->userModel->transRollback();
					$this->session->setFlashdata("error", "Não foi possível salvar paciente! Tente novamente!");

					return view('form_user',[
						'errors' => $this->userModel->errors()
					]);
				}

			}catch (\Exception $ex) {
				var_dump($ex);die('esc');
				$this->userModel->transRollback();
				$this->session->setFlashdata("error", "Não foi possível salvar paciente! Tente novamente!");
				return $this->response->redirect(site_url('/user/create'));
			}

			$this->userModel->transCommit();

			return $this->response->redirect(site_url('/'));
		}
	}

	/**
	 * Formulario de alteração de Paciente
	 * @author André Maciel Fagundes
	 * @version 1.0
	 * @package App\Controllers
	 */
	public function edit($id){
		$params = [$id];
		$db = \db_connect();

		$dados = $db->query('SELECT * FROM "user" JOIN "address" ON "address"."user_id" = "user"."id" WHERE "user"."id" = ?', $params)->getRowObject();
		$db->close();

		return view('edit_user',[
			'user' => $dados
		]);
	}

	/**
	 * Alteração de dados do Paciente
	 * @author André Maciel Fagundes
	 * @version 1.0
	 * @package App\Controllers
	 */
	public function changeUser(){

		if($this->request->getMethod() === 'post'){
			try{

				$this->userModel->transBegin();

				$userPost = $this->request->getPost('user');
				$user['id']				= $userPost['id_user'];
				$user['cpf'] 			= limparFormatacao($userPost['cpf']);
				$user['cns'] 			= limparFormatacao($userPost['cns']);
				$user['nome'] 			= $userPost['nome'];
				$user['dt_nascimento'] 	= $userPost['dt_nascimento'];
				$user['nome_mae'] 		= $userPost['nome_mae'];
					
				if($this->userModel->save($user)){

					$this->addressModel = new AddressModel();

					$addressPost = $this->request->getPost('address');
					$addressPost['id']			= $addressPost['id_endereco'];
					$addressPost['user_id']		= $userPost['id_user'];
					
					if($this->addressModel->saveAddress($addressPost)){
						$this->session->setFlashdata("message", "Paciente alterado com sucesso!");
					}else{
						$this->userModel->transRollback();
						$this->session->setFlashdata("error", "Não foi possível alterar os dados do paciente!");

						return view('edit_user',[
							'errors' => $this->userModel->errors()
						]);
					}
				}else{
					$this->userModel->transRollback();
					$this->session->setFlashdata("error", "Não foi possível alterar os dados do paciente!");

					return view('edit_user',[
						'errors' => $this->userModel->errors()
					]);
				}

			}catch (\Exception $ex) {
				$this->userModel->transRollback();
				$this->session->setFlashdata("error", "Não foi possível alterar os dados do paciente!");
				return view('edit_user',[
						'errors' => $this->userModel->errors()
					]);
			}

			$this->userModel->transCommit();

			return $this->response->redirect(site_url('/'));
		}
	}

	/**
	 * Exclusão de Paciente
	 * @author André Maciel Fagundes
	 * @version 1.0
	 * @package App\Controllers
	 */
	public function delete($id){
		try{
			if($this->userModel->delete($id)){
				$this->session->setFlashdata("message", "Registro excluído com sucesso!");
			}else{
				$this->session->setFlashdata("message", "Erro ao excluir registro, tente novamente!");
			}
		}catch (\Exception $ex) {
				$this->session->setFlashdata("error", "Não foi possível deletar paciente! Tente novamente!");
				return $this->response->redirect(site_url('/'));
		}

		return $this->response->redirect(site_url('/'));
	
	}
}
