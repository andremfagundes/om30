<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OM30 TESTE</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- JS only -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-5">
        <div class="container-fluid">
            <?php echo anchor(base_url("/"), 'OM30 TESTE', 
                [
                    'title'=>'Pacientes',
                    'class' => 'navbar-brand',
                    'role' => 'button'
                ]); 
            ?>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <?php echo anchor(base_url("/"), 'Pacientes', 
                    [
                        'title'=>'Pacientes',
                        'class' => 'nav-link active',
                        'role' => 'button'
                    ]); ?>
                </li>
                <li class="nav-item">
                <?php echo anchor(base_url("user/create"), 'Cadastro Pacientes', 
                    [
                        'title'=>'Cadastro Pacientes',
                        'class' => 'nav-link active',
                        'role' => 'button'
                    ]); ?>
                </li>
                </li>
            </ul>
            </div>
        </div>
    </nav>
    <?php $this->renderSection('section-body'); ?>
</body>
</html>