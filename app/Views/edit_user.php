    <?php $this->extend('layouts/default'); ?>
    <?php $this->section('section-body'); ?>
    <div class="container container-fluid col-8 mb-5 ">
       <?php if (session()->has('error')) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
               <?php echo session()->getFlashdata('error').'<br/>'; ?>
               <?php if(isset($errors)): ?>
                    <?php foreach($errors as $erro): ?>
                    <?php echo "<strong>{$erro}</strong><br/>" ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <?php echo form_open("user/change-user") ?>
            <div class="mb-3 col-6">
                <label for="foto" class="form-label">Foto</label>
                <input type="file" class="form-control" aria-label="file example" id="foto" name="user[foto]">
            </div>
            <div class="row mb-3 ">
                <div class="col-4">
                    <label for="cpf" class="form-label">CPF</label>
                    <input type="text" value="<?php echo isset($user->cpf)?$user->cpf:''; ?>" class="form-control" id="cpf" name="user[cpf]" required>
                </div>
                <div class="col-4">
                     <label for="cns" class="form-label">CNS</label>
                    <input type="text" value="<?php echo isset($user->cns)?$user->cns:''; ?>" class="form-control" id="cns" name="user[cns]" required>
                </div>
            </div>
            <div class="row mb-3 ">
                <div class="col-8">
                    <label for="nome" class="form-label">Nome Completo do Paciente</label>
                    <input type="text" value="<?php echo isset($user->nome)?$user->nome:''; ?>" class="form-control" id="nome" name="user[nome]" required>
                </div>
                <div class="col-3">
                    <label for="dt_nascimento" class="form-label">Data de Nascimento</label>
                    <input type="text" value="<?php echo isset($user->dt_nascimento)?formatarDataVisualizacao($user->dt_nascimento):''; ?>" class="form-control" id="dt_nascimento" name="user[dt_nascimento]" required>
                </div>
            </div>
            <div class="mb-3 col-8">
                <label for="nome_mae" class="form-label">Nome Completo do Mãe</label>
                <input type="text" value="<?php echo isset($user->nome_mae)?$user->nome_mae:''; ?>" class="form-control" id="nome_mae" name="user[nome_mae]" required>
            </div>
           
            <div class="mb-3 col-3">
                <label for="cep" class="form-label">CEP</label>
                <input type="text" value="<?php echo isset($user->cep)?$user->cep:''; ?>" class="form-control" id="cep" name="address[cep]"  required>
            </div>
            <div class="mb-3">
                <label for="rua" class="form-label">Rua</label>
                <input type="text" value="<?php echo isset($user->rua)?$user->rua:''; ?>" class="form-control" id="rua" name="address[rua]" required>
            </div>
            <div class="mb-3 col-6">
                <label for="complemento" class="form-label">Complemento</label>
                <input type="text" value="<?php echo isset($user->complemento)?$user->complemento:''; ?>" class="form-control" id="complemento" name="address[complemento]">
            </div>
            <div class="row mb-3">
                <div class="col-6">
                    <label for="bairro" class="form-label">Bairro</label>
                    <input type="text" value="<?php echo isset($user->bairro)?$user->bairro:''; ?>" class="form-control" id="bairro" name="address[bairro]" required>
                </div>
                <div class="col-4">
                    <label for="cidade" class="form-label">Cidade</label>
                    <input type="text" value="<?php echo isset($user->cidade)?$user->cidade:''; ?>" class="form-control" id="cidade" name="address[cidade]" required>
                </div>
                <div class="col-2">
                    <label for="estado" class="form-label">Estado</label>
                    <input type="text" value="<?php echo isset($user->estado)?$user->estado:''; ?>" class="form-control" id="estado" name="address[estado]" required>
                </div>
            </div>
            <input type="hidden" id="id_endereco" name="address[id_endereco]" value="<?php echo isset($user->id)?$user->id:''; ?>">
            <input type="hidden" id="id_user" name="user[id_user]" value="<?php echo isset($user->user_id)?$user->user_id:''; ?>">
            <button type="submit" class="btn btn-primary"><?php echo isset($user->id)?'Alterar':'Cadastrar'; ?></button>
        <?php echo form_close(); ?>
    </div>
    <!-- JS Pg -->
    <script src="<?php echo base_url('js/user.js'); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            User.init();
        });
    </script>
    <?php $this->endSection(); ?>
</body>
</html>