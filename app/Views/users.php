    <?php $this->extend('layouts/default'); ?>
    <?php $this->section('section-body'); ?>
    <div class="container container-fluid">
        <div id="top" class="row">
            <div class="col-md-3">
                <h2>Pacientes</h2>
            </div>

            <div class="col-md-6">
                <div class="input-group h2">
                    <input name="data[search]" class="form-control" id="search" type="text" placeholder="Pesquisar Pacientes">
                    <button class="btn btn-primary" type="button" id="button-addon2">Pesquisar</button>
                </div>
            </div>

            <div class="col-md-3">
                <?php echo anchor(base_url('user/create'), 'Novo Paciente', 
                    [
                        'title'=>'Novo Paciente',
                        'class' => 'btn btn-primary pull-right h2',
                        'role' => 'button'
                    ]); ?>
                </div>
            </div>
        <!-- /#top -->
        <hr />
        <?php if (session()->has('message')) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('message'); ?>
            </div>
        <?php endif; ?>
        <?php if (session()->has('error')) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('error'); ?>
            </div>
        <?php endif; ?>
        <div class="container table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>CPF</th>
                        <th>CNS</th>
                        <th>Nome</th>
                        <th>Data de Nascimento</th>
                        <th>Nome da Mãe</th>
                        <th>Endereço</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($users as $user): ?>
                    <tr>
                        <td class="col-1"><?php echo formatarCPF($user->cpf); ?></td>
                        <td class="col-1"><?php echo $user->cns; ?></td>
                        <td class="col-3"><?php echo $user->nome; ?></td>
                        <td class="col-1"><?php echo formatarDataVisualizacao($user->dt_nascimento); ?></td>
                        <td class="col-2"><?php echo $user->nome_mae; ?></td>
                        <td class="col-2"><?php echo "{$user->rua} - {$user->bairro} - {$user->estado}" ?></td>
                        <td class="col-2">
                            <?php echo anchor(base_url("user/edit/$user->id"), 'Alterar', 
                            [
                                'title'=>'Editar Paciente',
                                'class' => 'btn btn-primary mr-2',
                                'role' => 'button'
                            ]); ?>
                            <?php echo anchor(base_url("user/delete/$user->id"), 'Excluir', 
                            [
                                'onclick' => 'return confirma()',
                                'title'=>'Excluir Paciente',
                                'class' => 'btn btn-danger mr-2',
                                'role' => 'button'
                            ]); ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $pager->links(); ?>
        </div>

        <div id="bottom" class="row">

        </div>
        <!-- /#bottom -->
    </div>
    <!-- /#main -->
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script>
        function confirma(){
            if(!confirm('Deseja excluir o registro?')){
                return false;
            }
            return true;
        }
    </script>
    <?php $this->endSection(); ?>
</body>
</html>